# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :kaleidoscope, KaleidoscopeWeb.Endpoint,
  url: [host: "localhost"],
  live_view: [signing_salt: "F8EtQL7wXaL7awkPMb1E7/4hNuG3mLwT"],
  secret_key_base: "BHKCxHxnGu5/RwD7YbBh1FX0qdiREWn5eld8iRKmbyNDSe9eoc9U6DWsmckVNRuN",
  render_errors: [view: KaleidoscopeWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Kaleidoscope.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
