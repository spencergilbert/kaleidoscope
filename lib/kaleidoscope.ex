defmodule Kaleidoscope do
  @moduledoc """
  Kaleidoscope keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  alias Kaleidoscope.Sources
  alias Kaleidoscope.Sinks

  def list_topics do
  end

  def track_topic(opts) do
    {:ok, _pid} = DynamicSupervisor.start_child(BroadwaySupervisor, {Sinks.GenericSink, opts})
    {:ok, _pid} = DynamicSupervisor.start_child(BroadwaySupervisor, {Sources.Kafka, opts})
  end
end
