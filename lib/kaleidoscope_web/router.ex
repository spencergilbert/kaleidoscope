defmodule KaleidoscopeWeb.Router do
  use KaleidoscopeWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug Phoenix.LiveView.Flash
    plug Plug.Telemetry, event_prefix: [:kaleidoscope, :plug]
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", KaleidoscopeWeb do
    pipe_through :browser

    get "/", TopicController, :index
    get "/topics/:topic", TopicController, :show
  end
end
