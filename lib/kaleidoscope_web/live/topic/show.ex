defmodule KaleidoscopeWeb.Live.Topic.Show do
  @moduledoc """
  LiveView show functionality to show messages
  """

  use Phoenix.LiveView
  alias Kaleidoscope.PubSub

  def render(assigns) do
    Phoenix.View.render(KaleidoscopeWeb.TopicView, "show.html", data: assigns)
  end

  def mount(%{topic: topic}, socket) do
    if connected?(socket) do
      Phoenix.PubSub.subscribe(PubSub, topic)
    end

    logs = get_messages(topic)
    {:ok, assign(socket, %{topic: topic, messages: logs})}
  end

  def handle_info("update", socket) do
    logs = get_messages(socket.assigns.topic)
    socket = assign(socket, %{topic: socket.assigns.topic, messages: logs})
    {:noreply, socket}
  end

  defp get_messages(topic) do
    topic |> Kaleidoscope.Sinks.GenericSink.get_data()
  end
end
