defmodule KaleidoscopeWeb.Live.Topic.Index do
  @moduledoc """
  LiveView index functionality to show topics
  """

  use Phoenix.LiveView
  alias Kaleidoscope.PubSub

  require Logger

  def render(assigns) do
    Phoenix.View.render(KaleidoscopeWeb.TopicView, "index.html", data: assigns)
  end

  def mount(_assigns, socket) do
    if connected?(socket) do
      Phoenix.PubSub.subscribe(PubSub, "topics")
    end

    {:ok, assign(socket, %{topics: get_topics(), new_topic: nil})}
  end

  def handle_event("add-topic", %{"new_topic" => new_topic}, socket) do
    Kaleidoscope.track_topic(topic: new_topic, group: "kaleidoscope_#{new_topic}")
    {:noreply, assign(socket, %{topics: get_topics(), new_topic: nil})}
  end

  def handle_info("added", socket) do
    {:noreply, assign(socket, %{topics: get_topics()})}
  end

  defp get_topics do
    Registry.lookup(KaleidoscopeTopicRegistry, "topics")
    |> Enum.map(fn {_pid, topic} -> topic end)
  end
end
