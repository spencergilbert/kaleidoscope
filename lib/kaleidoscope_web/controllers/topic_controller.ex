defmodule KaleidoscopeWeb.TopicController do
  use KaleidoscopeWeb, :controller

  import Phoenix.LiveView.Controller

  def index(conn, _params) do
    live_render(conn, KaleidoscopeWeb.Live.Topic.Index, session: %{topics: []})
  end

  def show(conn, %{"topic" => topic}) do
    live_render(conn, KaleidoscopeWeb.Live.Topic.Show, session: %{topic: topic})
  end
end
