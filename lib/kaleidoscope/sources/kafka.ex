defmodule Kaleidoscope.Sources.Kafka do
  @moduledoc """
  Implements Kafka consumer and configuration for message processing
  """
  use Broadway

  alias Kaleidoscope.Config

  def start_link(opts) do
    topic = Keyword.get(opts, :topic)
    group = Keyword.get(opts, :group)
    host = Config.get!(["source", "kafka", "broker"], as: :string)
    port = Config.get!(["source", "kafka", "port"], as: :string)
    port = String.to_integer(port)

    kafka_config = [
      name: :"kaleidoscope_group_#{group}_#{topic}",
      brokers: [{host, port}],
      group: :"#{group}",
      topics: [topic],
      config: [
        begin_offset: :earliest
      ]
    ]

    Broadway.start_link(__MODULE__,
      name: :"kaleidoscope_#{group}_#{topic}",
      producers: [
        default: [
          module: {OffBroadway.Kafka.Producer, kafka_config},
          stages: 1
        ]
      ],
      processors: [
        default: [
          stages: 1
        ]
      ],
      batchers: [
        generic: [
          batch_size: 1,
          batch_timeout: 1000
        ]
      ],
      context: %{pid: Keyword.get(opts, :pid)}
    )
  end

  @impl true
  def handle_message(_processor, message, _context) do
    # send(context.pid, {:message, message})
    message
    |> Broadway.Message.put_batcher(:generic)
  end

  @impl true
  def handle_batch(:generic, messages, _batch_info, _context) do
    messages
    |> Enum.each(&Kaleidoscope.Sinks.GenericSink.add_message/1)

    messages
  end
end
