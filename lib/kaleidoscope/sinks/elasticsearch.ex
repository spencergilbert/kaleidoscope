defmodule Kaleidoscope.Sinks.Elasticsearch do
  @moduledoc """
  NOTE incomplete! definitely pre-alpha state
  Implements indexing and configuration for Elasticsearch sink 
  """
  require Logger
  require Protocol

  alias Kaleidoscope.Config

  Protocol.derive(Jason.Encoder, Elsa.Message)

  @doc """
  https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html

  Leverage bulk endpoint to index logs
  """
  def index(list) do
    health_check()
    create_index(list)
    insert(list)
  end

  defp insert(list) do
    host = Config.get!(["sink", "elasticsearch", "host"], as: :string)
    port = Config.get!(["sink", "elasticsearch", "port"], as: :string)

    list
    |> Enum.map(fn item ->
      {item,
       Mojito.post(
         "http://#{host}:#{port}/#{item.topic}/create/",
         [{"content-type", "application/json"}],
         Jason.encode!(item)
       )}
    end)
  end

  defp create_index(list) do
    host = Config.get!(["sink", "elasticsearch", "host"], as: :string)
    port = Config.get!(["sink", "elasticsearch", "port"], as: :string)

    list
    |> Enum.map(fn item -> {item.topic, Mojito.head("http://#{host}:#{port}/#{item.topic}")} end)
    |> Enum.map(fn {topic, {:ok, resp}} -> {topic, resp.status_code} end)
    |> Enum.map(fn {topic, status_code} -> {topic, exists?(status_code, topic)} end)
    |> Enum.map(fn {topic, bool} -> create_index(topic, bool) end)
  end

  defp health_check do
    host = Config.get!(["sink", "elasticsearch", "host"], as: :string)
    port = Config.get!(["sink", "elasticsearch", "port"], as: :string)
    {:ok, response} = Mojito.get("http://#{host}:#{port}/_cluster/health")

    response.body
    |> Jason.decode!()
    |> Map.fetch!("status")
  end

  defp exists?(status_code, topic) do
    case status_code do
      200 ->
        Logger.info("Elasticsearch index already exists", topic: topic)
        false

      _ ->
        true
    end
  end

  defp create_index(topic, bool) do
    host = Config.get!(["sink", "elasticsearch", "host"], as: :string)
    port = Config.get!(["sink", "elasticsearch", "port"], as: :string)

    case bool do
      true ->
        Logger.debug("PUT http://#{host}:#{port}/#{topic}")
        Mojito.put("http://#{host}:#{port}/#{topic}")

      false ->
        nil
    end
  end
end
