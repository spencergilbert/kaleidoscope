defmodule Kaleidoscope.Sinks.GenericSink do
  @moduledoc """
  Implements a generic sink for messages
  """
  use GenServer

  require Logger

  @list_size 100

  def start_link(opts) do
    topic = Keyword.get(opts, :topic)
    process_name = name_via_registry(topic)
    GenServer.start_link(__MODULE__, opts, name: process_name)
  end

  def add_message(%Broadway.Message{data: data}) do
    process_name = name_via_registry(data.topic)
    GenServer.call(process_name, {:add_message, data})
  end

  def get_data(topic) do
    process_name = name_via_registry(topic)
    GenServer.call(process_name, :get_data)
  end

  def init(opts) do
    topic = Keyword.get(opts, :topic)
    Registry.register(KaleidoscopeTopicRegistry, "topics", topic)
    {:ok, [], {:continue, :register}}
  end

  def handle_continue(:register, data) do
    notify_new_topic()
    {:noreply, data}
  end

  def handle_call(:get_data, _from, data) do
    {:reply, data, data}
  end

  def handle_call({:add_message, message}, _from, data) do
    data =
      [message | data]
      |> Enum.take(@list_size)

    notify_new_message(message)

    {:reply, data, data}
  end

  defp notify_new_message(message) do
    Phoenix.PubSub.broadcast(Kaleidoscope.PubSub, message.topic, "update")
  end

  defp notify_new_topic do
    Phoenix.PubSub.broadcast(Kaleidoscope.PubSub, "topics", "added")
  end

  defp name_via_registry(topic) do
    {:via, Registry, {KaleidoscopeRegistry, topic}}
  end
end
