defmodule Kaleidoscope.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  alias Kaleidoscope.Config

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the endpoint when the application starts
      Kaleidoscope.Config,
      KaleidoscopeWeb.Endpoint,
      {Registry, keys: :unique, name: KaleidoscopeRegistry},
      {Registry, keys: :duplicate, name: KaleidoscopeTopicRegistry},
      {DynamicSupervisor, strategy: :one_for_one, name: BroadwaySupervisor}
      # Starts a worker by calling: Kaleidoscope.Worker.start_link(arg)
      # {Kaleidoscope.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Kaleidoscope.Supervisor]
    result = Supervisor.start_link(children, opts)

    start_topic_tracker()

    result
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    KaleidoscopeWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp start_topic_tracker do
    Config.get!(["source", "kafka", "topics"], as: :string)
    |> String.split(",")
    |> Enum.each(&Kaleidoscope.track_topic(topic: &1, group: "kaleidoscope_#{&1}"))
  end
end
