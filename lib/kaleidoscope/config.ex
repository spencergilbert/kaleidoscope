defmodule Kaleidoscope.Config do
  @moduledoc """
  Implements Vapor for application configuration
  """

  use Vapor

  alias Vapor.Config
  alias Vapor.Provider.{File, Env}

  def start_link(_args \\ []) do
    config =
      Config.default()
      # TODO move to a more reasonable location
      |> Config.watch(File.with_name("kaleidoscope.toml"))
      |> Config.watch(Env.with_prefix("KALEIDOSCOPE"))

    Vapor.start_link(__MODULE__, config, name: __MODULE__)
  end
end
