defmodule Kaleidoscope.MixProject do
  use Mix.Project

  def project do
    [
      app: :kaleidoscope,
      version: "0.1.0",
      elixir: "~> 1.9",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Kaleidoscope.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      # Phoenix/Live View deps
      {:phoenix, "~> 1.4.9"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_view, "~> 0.3.0"},
      {:floki, ">= 0.0.0", only: :test},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},

      # HTTP client deps
      {:mojito, "~> 0.5.0"},

      # Broadway deps
      {:off_broadway_kafka, "~> 0.3.1"},

      # Config
      {:vapor, "~> 0.2.0"},

      # Norm deps
      {:stream_data, "~> 0.4"},
      {:norm, "~> 0.5"},

      # Testing/linting deps
      {:credo, "~> 1.1.3", only: [:dev, :test], runtime: false}
    ]
  end
end
